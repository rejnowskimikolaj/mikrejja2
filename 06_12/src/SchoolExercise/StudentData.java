package SchoolExercise;

public class StudentData {

	private String mother;
	private String father;
	public StudentData(String mother, String father) {
		this.mother = mother;
		this.father = father;
	}
	public String getMother() {
		return mother;
	}
	public void setMother(String mother) {
		this.mother = mother;
	}
	public String getFather() {
		return father;
	}
	public void setFather(String father) {
		this.father = father;
	}	
	
	
	
	
}
