package games;

public interface Playable extends Settable {

	public void run();
	public void game();
	
}
