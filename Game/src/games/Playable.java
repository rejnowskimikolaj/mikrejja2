package games;

public interface Playable {

	public void run();
	public void game();
	
}
