package zad10;

public class Word {
	
	private String polishWord;
	private String englishWord;
	public Word(String polisWord, String englishWord) {
		this.polishWord = polisWord;
		this.englishWord = englishWord;
	}
	public String getPolishWord() {
		return polishWord;
	}
	public void setPolishWord(String polisWord) {
		this.polishWord = polisWord;
	}
	public String getEnglishWord() {
		return englishWord;
	}
	public void setEnglishWord(String englishWord) {
		this.englishWord = englishWord;
	}
	
	
	
}
